import os

import time
import io
from contextlib import redirect_stdout

import numpy as np
import pandas as pd

from gatspy.periodic import LombScargleMultibandFast

import warnings
warnings.simplefilter('ignore', FutureWarning)
warnings.simplefilter('ignore', RuntimeWarning)
warnings.simplefilter('ignore', UserWarning)


def get_time_foraobject():
    train_series = pd.read_csv('../input/training_set.csv')

    o615 = train_series[train_series.object_id == 615]

    model = LombScargleMultibandFast(fit_period=True)

    model.optimizer.period_range = (0.2, 360)

    with redirect_stdout(io.StringIO()):
        start = time.perf_counter()
        model.fit(o615.mjd, o615.flux, o615.flux_err, o615.passband)
        end = time.perf_counter()
    print('time for one object: ', end - start)


def get_test_split(i_split):
    split_index_df = pd.read_csv('../proccesed-data/test_100split_index.csv')
    start = split_index_df.loc[1, 'start'] + 1
    in_end = split_index_df.loc[1, 'in_end'] + 1
    return pd.read_csv(
        '../input/test_set.csv',
        skiprows=lambda x: not (x == 0 or start <= x <= in_end))


def fit_period(object_group):
    model = LombScargleMultibandFast(fit_period=True)

    model.optimizer.period_range = (0.2, 360)

    model.fit(object_group.mjd, object_group.flux, object_group.flux_err,
              object_group.passband)

    return model.best_period, model.score(model.best_period)


def fit_split_to_csv(i_split):
    start = time.perf_counter()
    train_series = pd.read_csv(f'../proccesed-data/test_split_{i_split}.csv')
    period_df = pd.DataFrame({
        "object_id":
        train_series.object_id.unique(),
        "best_period":
        np.zeros(train_series.object_id.unique().shape[0]),
        "score":
        np.zeros(train_series.object_id.unique().shape[0])
    })
    with redirect_stdout(io.StringIO()):
        for i in period_df.index:
            bp, score = fit_period(train_series[train_series.object_id ==
                                                period_df.loc[i].object_id])
            period_df.loc[i, 'best_period'] = bp
            period_df.loc[i, 'score'] = score
    period_df.to_csv(
        f"../proccesed-data/test_periods_split{i_split}.csv", index=False)
    print(i_split, time.ctime(), time.perf_counter() - start)
