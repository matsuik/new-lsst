import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.metrics import log_loss
from sklearn.model_selection import StratifiedKFold
import gc
import os
import lightgbm as lgb
import itertools
import pickle, gzip
import glob
import importlib
import time

from joblib import dump, load

import load_data

from cv_sampler import *

np.warnings.filterwarnings('ignore')


def submit_scv(gal_clf, ex_clf, gal_df, ex_df):

    pred_proba = gal_clf.predict_proba(gal_df)

    proba_df = pd.DataFrame(columns=[
        'object_id', 'class_6', 'class_15', 'class_16', 'class_42', 'class_52',
        'class_53', 'class_62', 'class_64', 'class_65', 'class_67', 'class_88',
        'class_90', 'class_92', 'class_95', 'class_99'
    ])

    proba_df['object_id'] = gal_df.index
    classes = [6, 16, 53, 65, 92]
    for i in range(len(classes)):
        proba_df[f'class_{classes[i]}'] = pred_proba[:, i]
    proba_df.fillna(0, inplace=True)

    pred_proba = ex_clf.predict_proba(ex_df)

    ex_proba_df = pd.DataFrame(columns=[
        'object_id', 'class_6', 'class_15', 'class_16', 'class_42', 'class_52',
        'class_53', 'class_62', 'class_64', 'class_65', 'class_67', 'class_88',
        'class_90', 'class_92', 'class_95', 'class_99'
    ])
    ex_proba_df['object_id'] = ex_df.index
    classes = [15, 42, 52, 62, 64, 67, 88, 90, 95]
    for i in range(len(classes)):
        ex_proba_df[f'class_{classes[i]}'] = pred_proba[:, i]
    ex_proba_df.fillna(0, inplace=True)

    return pd.concat([proba_df, ex_proba_df], ignore_index=True)


def write_submit_unknown_onehot(subdf, filename, only_proba=True):
    subdf.to_csv(
        os.path.join('../results', filename + '_raw_proba.csv'), index=False)
    if not only_proba:
        onehot_df = pd.DataFrame(0, index=subdf.index, columns=subdf.columns)
        for i, key in enumerate(
                subdf.drop(columns=['object_id']).idxmax(axis=1)):
            onehot_df.at[i, key] = 1
        onehot_df.object_id = subdf.object_id

        onehot_df.to_csv(
            os.path.join('../results', filename + '_raw_onehot.csv'),
            index=False)

        unknown_df = subdf.copy()
        for i, max_ in enumerate(
                subdf.drop(columns=['object_id']).max(axis=1)):
            if max_ < 0.4:
                unknown_df.at[i, 'class_99'] = 1 - max_

        onehot_df = pd.DataFrame(0, index=subdf.index, columns=subdf.columns)
        for i, key in enumerate(
                unknown_df.drop(columns=['object_id']).idxmax(axis=1)):
            onehot_df.at[i, key] = 1
        onehot_df.object_id = subdf.object_id

        unknown_df.to_csv(
            os.path.join('../results', filename + '_unknown.csv'), index=False)
        onehot_df.to_csv(
            os.path.join('../results', filename + '_unknown_onehot.csv'),
            index=False)


def test_submit(i_split, filename, gal_clf_path, ex_clf_path):
    t_period_df = pd.read_csv(
        f'../proccesed-data/test_periods_split{i_split}.csv')

    t_period_df.index = t_period_df.object_id
    t_period_df.drop(columns=['object_id'], inplace=True)

    test_df = pd.read_csv(f'../proccesed-data/test_split_{i_split}.csv')
    test_feature = load_data.featurize(test_df)
    concat_df = pd.concat([test_feature, t_period_df], axis=1)

    meta_data = pd.read_csv(
        '../input/test_set_metadata.csv',
        usecols=['object_id', 'hostgal_photoz'])
    meta_data.index = meta_data.object_id

    gal_df, ex_df = load_data.cut_galaxy(concat_df,
                                         meta_data.loc[concat_df.index])
    gal_clf = load(gal_clf_path)
    ex_clf = load(ex_clf_path)

    submit_df = submit_scv(gal_clf, ex_clf, gal_df, ex_df)

    write_submit_unknown_onehot(submit_df, filename)


def post_process(submit_df):
    submit_df = submit_df.drop_duplicates('object_id')
    submit_df.clip(0.01, inplace=True)
    submit_df.object_id = submit_df.object_id.astype(int)
    submit_df.class_99 = submit_df.drop(
        columns=['object_id', 'class_99']).apply(lambda x: 1 - x).prod(axis=1)
    return submit_df