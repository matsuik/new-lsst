import pandas as pd
import numpy as np


def featurize(df):
    flux_all_dict = {
        "flux": ["min", "max", "mean", "median", "std", "skew"],
        "flux_err": ["min", "std"],
        "detected": ["mean"]
    }

    flux_all_feats = df.groupby('object_id').agg(flux_all_dict)
    new_columns = [
        k + '_' + agg for k in flux_all_dict.keys() for agg in flux_all_dict[k]
    ]
    flux_all_feats.columns = new_columns

    flux_kurt = df.groupby('object_id').flux.apply(lambda x: x.kurtosis())
    flux_all_feats['flux_kurtosis'] = flux_kurt

    flux_all_feats[
        'flux_diff'] = flux_all_feats['flux_max'] - flux_all_feats['flux_min']

    flux_all_feats['flux_abs_median'] = df.groupby('object_id').flux.apply(
        lambda x: x.abs().median())
    flux_all_feats['flux_diff_dbmed'] = flux_all_feats[
        'flux_diff'] / flux_all_feats['flux_abs_median']
    flux_all_feats['flux_max_dbmed'] = flux_all_feats[
        'flux_max'] / flux_all_feats['flux_abs_median']
    flux_all_feats['flux_min_dbmed'] = flux_all_feats[
        'flux_min'] / flux_all_feats['flux_abs_median']

    flux_pb_feats = df.groupby(['object_id', 'passband']).agg({
        "flux": ["min", "max", "mean", "median", "std", "skew"],
    })

    flux_pb_feats.columns = [
        'flux_' + measure for measure in flux_pb_feats.columns.levels[1]
    ]

    flux_pb_kurt = df.groupby(['object_id',
                               'passband']).flux.apply(lambda x: x.kurtosis())
    flux_pb_absmed = df.groupby(
        ['object_id', 'passband']).flux.apply(lambda x: x.abs().median())

    concat_pd_feats = pd.concat([
        flux_pb_feats,
        pd.DataFrame({
            'flux_kurt': flux_pb_kurt
        }),
        pd.DataFrame({
            'flux_absmed': flux_pb_absmed
        })
    ],
                                axis=1)

    concat_pd_feats["flux_diff"] = concat_pd_feats[
        "flux_max"] - concat_pd_feats["flux_min"]
    concat_pd_feats["flux_diff_dbmed"] = concat_pd_feats[
        "flux_diff"] / concat_pd_feats['flux_absmed']
    concat_pd_feats["flux_max_dbmed"] = concat_pd_feats[
        "flux_max"] / concat_pd_feats['flux_absmed']
    concat_pd_feats["flux_min_dbmed"] = concat_pd_feats[
        "flux_min"] / concat_pd_feats['flux_absmed']

    flat_concat_pd_feats = concat_pd_feats.unstack()
    flat_concat_pd_feats.columns = [
        key + "_" + str(pb) for key in flat_concat_pd_feats.columns.levels[0]
        for pb in flat_concat_pd_feats.columns.levels[1]
    ]

    return pd.concat([flux_all_feats, flat_concat_pd_feats], axis=1)


def cut_galaxy(object_grouped_df, meta_df):
    gal_cut = meta_df.hostgal_photoz == 0
    return object_grouped_df[gal_cut], object_grouped_df[~gal_cut]


def load_train_period():
    period_df = pd.read_csv('../proccesed-data/train_periods.csv')
    pdf = period_df.drop(columns=['object_id', 'Unnamed: 0'])
    pdf.index = period_df.object_id
    return pdf


def load_train():
    train_df = pd.read_csv('../input/training_set.csv')
    feature_df = featurize(train_df)
    period_df = load_train_period()
    concat_df = pd.concat([feature_df, period_df], axis=1)
    meta_data = pd.read_csv('../input/training_set_metadata.csv')
    meta_data.index = meta_data.object_id
    gal_df, ex_df = cut_galaxy(concat_df, meta_data)
    gal_target, ex_target = cut_galaxy(meta_data.target, meta_data)

    return gal_df, ex_df, gal_target, ex_target


def get_weights(gal_target):
    gal_classes = sorted(gal_target.unique())
    gal_class_weight = {
        i: (2 if c in [15, 64] else 1)
        for i, c in enumerate(gal_classes)
    }
    gal_w = gal_target.value_counts()
    gal_sample_weights = {
        i: np.sum(gal_w) / gal_w[c]
        for i, c in enumerate(gal_classes)
    }
    gal_target_i = gal_target.apply(lambda x: gal_classes.index(x))

    return gal_class_weight, gal_sample_weights, gal_target_i